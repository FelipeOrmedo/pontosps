﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace PontoSPSWeb.WebForms
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            Dao.LoginDao login = new Dao.LoginDao();
            login.Email = Login1.UserName;
            login.Password = Login1.Password;

            if(login.Login())
            {
                e.Authenticated = true;
                FormsAuthentication.RedirectFromLoginPage(Login1.UserName, false);
                Response.Redirect("~/WebForms/Dashboard.aspx");
                
            }else
            {
                e.Authenticated = false;
            }
        }
    }
}
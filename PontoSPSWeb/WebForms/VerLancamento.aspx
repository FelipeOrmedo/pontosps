﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="VerLancamento.aspx.cs" Inherits="PontoSPSWeb.WebForms.VerLancamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h4 class="page-title titulo">Visão Lançamento</h4>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-lg-12 card-box">
            <%--MENSAGEM PARA O USUÁRIO--%>
            <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-6">
                            <asp:Label ID="mensagemLabel" runat="server" Text=""></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <%--BOTÃO BUSCAR--%>
            <div class="row ">
                <div class="col-lg-12">
                    <asp:Label ID="Label1" runat="server" Text="Label" CssClass="text-muted page-title-alt left margin">Funcionario: </asp:Label>
                    <div class="col-lg-5">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="funcionariosDropDownList" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--<asp:TextBox ID="txtNome" runat="server" class="form-control left"></asp:TextBox>--%>
                    </div>
                    <asp:Label ID="Label3" runat="server" Text="Label" CssClass="text-muted page-title-alt left margin">Periodo: </asp:Label>
                    <div class="col-lg-2">
                        <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server">
                            <asp:ListItem Value="01-2017"></asp:ListItem>
                            <asp:ListItem Value="02-2017"></asp:ListItem>
                            <asp:ListItem Value="03-2017"></asp:ListItem>
                            <asp:ListItem Value="04-2017"></asp:ListItem>
                            <asp:ListItem Value="05-2017"></asp:ListItem>
                            <asp:ListItem Value="06-2017"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default waves-effect waves-light BtnVerLanc" EnableTheming="False" EnableViewState="False" OnClick="LinkButton1_Click">Buscar</asp:LinkButton>
                </div>
            </div>

            <div class="row marginTop">
                <div class="col-lg-12 ">
                    <asp:Label ID="funcionarioLabel" runat="server" Text=""></asp:Label>
                </div>
            </div>

            <%--GRID COM INFORMAÇÔES--%>
            <div class="p-20">
                <table class="table table-striped m-0">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Hora Entrada</th>
                            <th>Saída Almoço</th>
                            <th>Retorno Almoço</th>
                            <th>Hora Saída</th>
                            <th>Rendimento</th>
                            <th>Desconto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Label ID="linhasLabel" runat="server" Text=""></asp:Label>
                    </tbody>
                </table>
                <div class="row marginTop">
                    <div class="col-lg-12">
                        <div class="col-lg-3 right">
                            <asp:Label ID="Label2" runat="server" CssClass="text-muted page-title-alt left margin" Text="Total de Horas:"></asp:Label>
                            <div class="col-lg-6">
                                <asp:TextBox ID="txtTotalHoras" runat="server" class="form-control right" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3 right">
                            <asp:Label ID="Label4" runat="server" CssClass="text-muted page-title-alt left margin" Text="Rendimento Total:"></asp:Label>
                            <div class="col-lg-5">
                                <asp:TextBox ID="txtTotalRendimento" runat="server" class="form-control right" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3 right">
                            <asp:Label ID="Label5" runat="server" CssClass="text-muted page-title-alt left margin" Text="Desconto Total:"></asp:Label>
                            <div class="col-lg-5">
                                <asp:TextBox ID="txtTotalDesconto" runat="server" class="form-control right" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="LancManual.aspx.cs" Inherits="PontoSPSWeb.WebForms.LancManual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.maskedinput.js"> </script>
    <script>
        $(function () {
            $('#ContentPlaceHolder1_txtDia').mask('99/99/9999');
            $('#ContentPlaceHolder1_txtEntrada').mask('99:99:99');
            $('#ContentPlaceHolder1_txtSaidaAlmoco').mask('99:99:99');
            $('#ContentPlaceHolder1_txtRetornoAlmoco').mask('99:99:99');
            $('#ContentPlaceHolder1_txtSaida').mask('99:99:99');
        });
    </script>

    <script>
        function topo() {
            document.write("Teste");
        }
    </script>

     <link rel="stylesheet" href="../plugins/magnific-popup/css/magnific-popup.css" />
    <link rel="stylesheet" href="../plugins/jquery-datatables-editable/datatables.css" />

    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
    <link href="../css/components.css" rel="stylesheet" type="text/css" />
    <link href="../css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../css/menu.css" rel="stylesheet" type="text/css" />
    <link href="../css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="../js/modernizr.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h4 class="page-title titulo">Visão Lançamento</h4>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="row">
        <div class="col-lg-12 card-box">

            <%--BOTÃO BUSCAR--%>
            <div class="row ">
                <div class="col-lg-12">
                    <asp:Label ID="Label1" runat="server" Text="Label" CssClass="text-muted page-title-alt left margin">Funcionario: </asp:Label>
                    <div class="col-lg-5">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="funcionariosDropDownList" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <asp:Label ID="Label3" runat="server" Text="Label" CssClass="text-muted page-title-alt left margin">Periodo: </asp:Label>
                    <div class="col-lg-2">
                        <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server">
                            <asp:ListItem Value="01-2017"></asp:ListItem>
                            <asp:ListItem Value="02-2017"></asp:ListItem>
                            <asp:ListItem Value="03-2017"></asp:ListItem>
                            <asp:ListItem Value="04-2017"></asp:ListItem>
                            <asp:ListItem Value="05-2017"></asp:ListItem>
                            <asp:ListItem Value="06-2017"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default waves-effect waves-light BtnVerLanc" EnableTheming="False" EnableViewState="False" OnClick="LinkButton1_Click">Buscar</asp:LinkButton>
                </div>
            </div>

            <div class="row marginTop">
                <div class="col-lg-12 ">
                    <asp:Label ID="funcionarioLabel" runat="server" Text=""></asp:Label>
                </div>
            </div>

            <div class="row">
                <asp:UpdatePanel ID="Updatepanel" runat="server">
                    <ContentTemplate>

                        <div class="col-lg-12">
                            <asp:Label ID="Label2" runat="server" Text="Label" CssClass="text-muted page-title-alt left margin-flt-lanc">Dia: </asp:Label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtDia" runat="server" class="form-control left"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <asp:Label ID="Label4" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Entrada: </asp:Label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtEntrada" runat="server" class="form-control left"></asp:TextBox>
                            </div>
                            <asp:Label ID="Label5" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Saída Almoço: </asp:Label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtSaidaAlmoco" runat="server" class="form-control left"></asp:TextBox>
                            </div>
                            <asp:Label ID="Label6" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Retorno Almoço: </asp:Label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtRetornoAlmoco" runat="server" class="form-control left"></asp:TextBox>
                            </div>
                            <asp:Label ID="Label7" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Saída: </asp:Label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtSaida" runat="server" class="form-control left"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <asp:Label ID="Label8" runat="server" Text="Label" CssClass="text-muted page-title-alt left margin-flt-lanc-obs">OBS: </asp:Label>
                            <div class="col-lg-11">
                                <asp:TextBox ID="txtObs" runat="server" class="form-control left"></asp:TextBox>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <%--MENSAGEM PARA O USUÁRIO--%>
                <div class="row">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="col-lg-6">
                                <asp:Label ID="mensagemLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-12">
                            <asp:LinkButton ID="salvarLinkButton" Visible="false" runat="server" CssClass="btn btn-success waves-effect waves-light" OnClick="salvarLinkButton_Click">Salvar</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

            <%--GRID COM INFORMAÇÔES--%>
            <div class="row topTen">
                <div class="col-lg-12">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-striped m-0" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField HeaderText="Ação" ShowSelectButton="True" ControlStyle-CssClass="btn btn-purple waves-effect waves-light btn-xs" SelectText="Editar">
                                        <ControlStyle CssClass="btn btn-purple waves-effect waves-light btn-xs" />
                                        <ItemStyle Width="50px" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="Data" HeaderText="Data" />
                                    <asp:BoundField DataField="hrEntrada" HeaderText="Hora Entrada" />
                                    <asp:BoundField DataField="hrSaidaAlmoco" HeaderText="Saída Almoço" />
                                    <asp:BoundField DataField="hrRetornoAlmoco" HeaderText="Retorno Almoço" />
                                    <asp:BoundField DataField="hrHoraSaida" HeaderText="Hora Saída" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <!-- jQuery  -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/detect.js"></script>
    <script src="../js/fastclick.js"></script>
    <script src="../js/jquery.slimscroll.js"></script>
    <script src="../js/jquery.blockUI.js"></script>
    <script src="../js/waves.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/jquery.nicescroll.js"></script>
    <script src="../js/jquery.scrollTo.min.js"></script>

    <!-- Examples -->
    <script src="../plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <script src="../plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="../plugins/tiny-editable/mindmup-editabletable.js"></script>
    <script src="../plugins/tiny-editable/numeric-input-example.js"></script>

    <script src="../pages/datatables.editable.init.js"></script>

    <!-- App core js -->
    <script src="../js/jquery.core.js"></script>
    <script src="../js/jquery.app.js"></script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Importacao.aspx.cs" Inherits="PontoSPSWeb.WebForms.Importacao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Plugins css -->
    <link href="../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" />
    <link href="../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <link href="../plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet" />
    <link href="../plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <script src="../js/modernizr.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h4 class="page-title titulo">Importação de arquivo</h4>

    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <div class="row">
        <div class="col-lg-12 card-box">

            <%--MENSAGEM PARA O USUÁRIO--%>
            <div class="row">
                <asp:updatepanel id="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-6">
                            <asp:Label ID="mensagemLabel" runat="server" Text=""></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:updatepanel>
            </div>
            
            <%--LINHA DE ATE--%>
            <div class="row">
                <div class="col-lg-12 input-group">
                    <%--<asp:label id="Label3" runat="server" text="Label" cssclass="text-muted page-title-alt left margin">Data de:</asp:label>--%>
                    <div class="col-sm-6">
                        <div class="input-daterange input-group" id="date-range">
                            <span class="input-group-addon bg-custom b-0 text-white">Data de:</span>
                            <asp:textbox runat="server" CssClass="form-control" name="start" ID="start" placeholder="Data Inicio"></asp:textbox>
                            <span class="input-group-addon bg-custom b-0 text-white">até</span>
                            <asp:textbox runat="server" CssClass="form-control" name="end" ID="end" placeholder="Data Fim"></asp:textbox>
                        </div>
                    </div>
                </div>
            </div>            

             <%--UPLOAD FILE--%>
            <div class="row">
                <asp:label id="Label1" runat="server" text="Label" cssclass="text-muted page-title-alt left margin">Selecionar arquivo: </asp:label>
                <div class="filUpload">
                    <asp:fileupload id="filUpload" runat="server" cssclass="bottom-left" />
                </div>
            </div>

            <%--BOTÃO IMPORTAR ARQUIVO--%>
            <div class="row ">
                <div class="col-lg-12">
                    <asp:linkbutton id="LinkButton1" runat="server" cssclass="btn btn-default waves-effect waves-light alinhaBtn left btnImport" onclick="LinkButton1_Click" enabletheming="False" enableviewstate="False">Importar Arquivo</asp:linkbutton>
                    <asp:Image ID="loadImage" CssClass="loadImage" runat="server" ImageUrl="../Imagens/load.gif"/>
                </div>
            </div>
        </div>        
    </div>    

    <!-- jQuery  -->
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/detect.js"></script>
        <script src="../js/fastclick.js"></script>
        <script src="../js/jquery.slimscroll.js"></script>
        <script src="../js/jquery.blockUI.js"></script>
        <script src="../js/waves.js"></script>
        <script src="../js/wow.min.js"></script>
        <script src="../js/jquery.nicescroll.js"></script>
        <script src="../js/jquery.scrollTo.min.js"></script>

        <!-- plugins js -->
        <script src="../plugins/moment/moment.js"></script>
     	<script src="../plugins/timepicker/bootstrap-timepicker.js"></script>
     	<script src="../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
     	<script src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
     	<script src="../plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
     	<script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- App core js -->
        <script src="../js/jquery.core.js"></script>
        <script src="../js/jquery.app.js"></script>

        <!-- page js -->
        <script src="../pages/jquery.form-pickers.init.js"></script>

</asp:Content>

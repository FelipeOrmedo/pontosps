﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PontoSPSWeb.WebForms
{
    public partial class CadUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dao.CadUsuarioDao.CarregaGridView(GridView1);
            }
        }

        protected void salvarLinkButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCod.Text))
            {
                if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtEmail.Text))
                {
                    mensagemLabel.Text = Support.SystemMessage.Info("Campos Nome e Email Obrigatórios");
                    return;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtSenha.Text))
                {
                    mensagemLabel.Text = Support.SystemMessage.Info("Preenchimento de todos os campos obrigatórios");
                    return;
                }
            }


            Model.CadUsuarioModel model = new Model.CadUsuarioModel();
            if (!string.IsNullOrEmpty(txtCod.Text))
                model.ID = Convert.ToInt32(txtCod.Text);

            model.Nome = txtNome.Text;
            model.Email = txtEmail.Text;
            model.Senha = Support.Criptografy.GerarHashMd5(txtSenha.Text);

            try
            {
                if (string.IsNullOrEmpty(txtCod.Text))
                    Dao.CadUsuarioDao.Save(model);
                else
                    Dao.CadUsuarioDao.Update(model);

                mensagemLabel.Text = Support.SystemMessage.Sucess("Informações salva com sucesso");
                Dao.CadUsuarioDao.CarregaGridView(GridView1);
            }
            catch (Exception ex)
            {
                mensagemLabel.Text = Support.SystemMessage.Danger("Erro: " + ex.Message);
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mensagemLabel.Text = string.Empty;
            if (IsPostBack)
            {
                excluirLinkButton.Visible = true;
                txtCod.Text = GridView1.SelectedRow.Cells[1].Text;
                txtNome.Text = GridView1.SelectedRow.Cells[2].Text;
                txtEmail.Text = GridView1.SelectedRow.Cells[3].Text;
            }
        }

        protected void cancelarLinkButton_Click(object sender, EventArgs e)
        {
            LimpaCampos();
        }

        protected void excluirLinkButton_Click(object sender, EventArgs e)
        {
            Model.CadUsuarioModel model = new Model.CadUsuarioModel();
            model.ID = Convert.ToInt32(txtCod.Text);

            try
            {
                Dao.CadUsuarioDao.Delete(model);
                mensagemLabel.Text = Support.SystemMessage.Sucess("Informações excluida com sucesso");
                Dao.CadUsuarioDao.CarregaGridView(GridView1);
            }
            catch (Exception ex)
            {
                mensagemLabel.Text = Support.SystemMessage.Danger("Erro: " + ex.Message);
            }
            LimpaCampos();
        }

        private void LimpaCampos()
        {
            txtCod.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtSenha.Text = string.Empty;
            excluirLinkButton.Visible = false;
        }
    }
}
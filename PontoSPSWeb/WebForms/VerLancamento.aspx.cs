﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PontoSPSWeb.WebForms
{
    public partial class VerLancamento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dao.VerLancamentoDao.BuscaFuncionarios(funcionariosDropDownList);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Model.VerLancamentoModel.TotalHoras = "00:00:00";
            Model.VerLancamentoModel.TotalRendimento = "00:00:00";
            Model.VerLancamentoModel.TotalDesconto = "00:00:00";

            funcionarioLabel.Text = string.Empty;
            linhasLabel.Text = string.Empty;
            if (string.IsNullOrEmpty(funcionariosDropDownList.SelectedItem.Text))
            {
                funcionarioLabel.Text = Support.StaticMessage.Danger("Selecione um funcionário para realizar a busca");
                return;
            }

            string nome = funcionariosDropDownList.SelectedItem.Text;
            linhasLabel.Text = Dao.VerLancamentoDao.Busca(nome , DropDownList1.SelectedValue);

            funcionarioLabel.Text = Support.StaticMessage.Info("<strong>FUNCIONÁRIO: </strong>" + funcionariosDropDownList.SelectedItem.Text);

            txtTotalHoras.Text = Model.VerLancamentoModel.TotalHoras;
            txtTotalRendimento.Text = Model.VerLancamentoModel.TotalRendimento;
            txtTotalDesconto.Text = Model.VerLancamentoModel.TotalDesconto;
        }


    }
}
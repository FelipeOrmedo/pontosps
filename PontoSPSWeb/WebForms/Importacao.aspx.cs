﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PontoSPSWeb.WebForms
{
    public partial class Importacao : System.Web.UI.Page
    {
        public string mensagem = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadImage.Visible = false;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            loadImage.Visible = true;
            //Verifica se tem alguma coisa postada 
            if (this.filUpload.PostedFile.ContentLength != 0 && this.filUpload.HasFile)
            {
                Model.ImportModel Model = new Model.ImportModel();

                //string dtDe = Convert.ToDateTime(start.Text).ToString("ddMMyyyy");
                string dtDe = Convert.ToDateTime(start.Text).ToString("dd/MM/yyyy");
                string dtAte = Convert.ToDateTime(end.Text).ToString("dd/MM/yyyy");

                //capturando nome original do arquivo
                string fileName = this.filUpload.FileName;

                //capturando extensão do arquivo postado
                string extension = System.IO.Path.GetExtension(fileName);

                //verificando se o arquivo escolhido é do tipo TXT
                if (!extension.Equals(".txt", StringComparison.OrdinalIgnoreCase))
                {
                    mensagemLabel.Text = Support.SystemMessage.Info("Selecione um arquivo do tipo TXT para realizar a importação");
                }
                else
                {
                    //Definindo o caminho do arquivo para ser salvo no servidor
                    string vCamArq = Path.GetTempPath() + fileName;

                    //Salvando o arquivo com o nome original
                    this.filUpload.PostedFile.SaveAs(vCamArq);

                    //Cria um novo arquivo e passa para o objeto StreamWriter
                    StreamReader Leitura = new StreamReader(vCamArq, System.Text.Encoding.GetEncoding(1252));
                    //variavel para receber as linhas
                    string strLinha;
                    //loop de leitura, linha por linha

                    bool cabecalho = false;
                    string CompNome = string.Empty;
                    DateTime CompData = DateTime.MinValue;
                    int contador = 0;
                    while (Leitura.Peek() != -1)
                    {
                        //lendo a linha atual
                        strLinha = Leitura.ReadLine();
                        string[] linhaSeparada = null;


                        if (strLinha.Substring(0, 1).Contains("N"))
                            cabecalho = true;
                        else
                            cabecalho = false;

                        //verificando se a linha esta vazia
                        if (strLinha.Trim().Length > 0 && cabecalho == false)
                        {
                            strLinha = strLinha.Replace("  ", ";").Replace("   ", ";").Replace("    ", ";").Replace("     ", ";");

                            linhaSeparada = strLinha.Split(';');

                            int IdUserArquivo = Convert.ToInt32(linhaSeparada[0].ToString().Trim());
                            int mchn = Convert.ToInt32(linhaSeparada[2].ToString().Trim());
                            string numPonto = linhaSeparada[3].ToString().Trim();
                            string nome = linhaSeparada[5].ToString().Trim();
                            string imld = linhaSeparada[6].ToString().Trim();
                            string mode = linhaSeparada[7].ToString().Trim() == "0" ? "A" : "M";
                            DateTime dataPonto = Convert.ToDateTime(string.Concat(linhaSeparada[8].ToString().Trim(), " ", linhaSeparada[9].ToString().Trim()));

                            if (contador == 0)
                            {
                                CompNome = nome;
                                CompData = dataPonto;
                                contador++;
                            }
                            else
                            {
                                if (CompNome == nome && Convert.ToString(CompData).Substring(3, 13) == Convert.ToString(dataPonto).Substring(3, 13))
                                {
                                    CompNome = nome;
                                    CompData = dataPonto;
                                    continue;
                                }

                                CompNome = nome;
                                CompData = dataPonto;
                            }

                            string data = Convert.ToDateTime(linhaSeparada[8]).ToString("dd/MM/yyyy").Trim();
                            if (Convert.ToDateTime(data) >= Convert.ToDateTime(dtDe) && Convert.ToDateTime(data) <= Convert.ToDateTime(dtAte))
                            {
                                try
                                {
                                    Model.IdUserArquivo = IdUserArquivo;
                                    Model.Mchn = mchn;
                                    Model.NumPonto = numPonto;
                                    Model.Nome = nome;
                                    Model.IOMd = imld;
                                    Model.Mode = mode;
                                    Model.Data = dataPonto;

                                    if (Dao.PontoDao.ValidaExiste(Model))
                                        Dao.PontoDao.Update(Model);
                                    else
                                        Dao.PontoDao.Save(Model);
                                }
                                catch (Exception ex)
                                {
                                    mensagemLabel.Text = Support.SystemMessage.Danger("Erro ao importar: " + ex.Message);
                                }
                            }
                        }
                    }
                    //fechando o arquivo
                    Leitura.Close();
                    loadImage.Visible = false;
                    mensagemLabel.Text = Support.SystemMessage.Sucess("Arquivo importado com Sucesso");
                }
            }
            else
            {
                mensagemLabel.Text = Support.SystemMessage.Alert("Selecione um arquivo para realizar a importação!");
            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Funcionario.aspx.cs" Inherits="PontoSPSWeb.WebForms.Funcionario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="../plugins/magnific-popup/css/magnific-popup.css" />
    <link rel="stylesheet" href="../plugins/jquery-datatables-editable/datatables.css" />

    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
    <link href="../css/components.css" rel="stylesheet" type="text/css" />
    <link href="../css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../css/menu.css" rel="stylesheet" type="text/css" />
    <link href="../css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    <script src="../js/modernizr.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h4 class="page-title titulo">Controle de Funcionários</h4>

    <div class="row">
        <div class="col-lg-12 card-box">

            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Label ID="mensagemLabel" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>                    

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Label ID="Label1" runat="server" Text="Label" CssClass="text-muted page-title-alt left" >ID: </asp:Label>
                            <div class="col-lg-1">
                                <asp:TextBox ID="txtCod" runat="server" class="form-control left" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Label ID="Label4" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Cód: </asp:Label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtCode" runat="server" class="form-control left marginRightfunc"></asp:TextBox>

                            </div>
                            <asp:Label ID="Label2" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Nome: </asp:Label>
                            <div class="col-lg-4">
                                <asp:TextBox ID="txtNome" runat="server" class="form-control left marginRightfunc"></asp:TextBox>
                            </div>

                            <asp:Label ID="Label3" runat="server" Text="Label" CssClass="text-muted page-title-alt left">Setor: </asp:Label>
                            <div class="col-lg-4">
                                <asp:TextBox ID="txtSetor" runat="server" class="form-control left marginRightfunc"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <asp:LinkButton ID="salvarLinkButton" runat="server" CssClass="btn btn-success waves-effect waves-light" OnClick="salvarLinkButton_Click">Salvar</asp:LinkButton>
                            <asp:LinkButton ID="excluirLinkButton" runat="server" CssClass="btn btn-primary waves-effect waves-light" OnClick="excluirLinkButton_Click" Visible="False">Excluir</asp:LinkButton>
                            <asp:LinkButton ID="cancelarLinkButton" runat="server" CssClass="btn btn-danger waves-effect waves-light" OnClick="cancelarLinkButton_Click">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="row marginTop">
                        <div class="col-lg-12">
                            <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-striped m-0" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField HeaderText="Ação" ShowSelectButton="True" ControlStyle-CssClass="btn btn-purple waves-effect waves-light btn-xs" SelectText="Selecionar">
                                        <ControlStyle CssClass="btn btn-purple waves-effect waves-light btn-xs" />
                                        <ItemStyle Width="100px" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="IdFuncionario" HeaderText="ID" />
                                    <asp:BoundField DataField="Codigo" HeaderText="Código" />
                                    <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                    <asp:BoundField DataField="Cargo" HeaderText="Setor" />
                                </Columns>
                            </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
        </div>
    </div>

    <!-- jQuery  -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/detect.js"></script>
    <script src="../js/fastclick.js"></script>
    <script src="../js/jquery.slimscroll.js"></script>
    <script src="../js/jquery.blockUI.js"></script>
    <script src="../js/waves.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/jquery.nicescroll.js"></script>
    <script src="../js/jquery.scrollTo.min.js"></script>

    <!-- Examples -->
    <script src="../plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <script src="../plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="../plugins/tiny-editable/mindmup-editabletable.js"></script>
    <script src="../plugins/tiny-editable/numeric-input-example.js"></script>

    <script src="../pages/datatables.editable.init.js"></script>

    <!-- App core js -->
    <script src="../js/jquery.core.js"></script>
    <script src="../js/jquery.app.js"></script>

    <script>
        $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
    </script>
</asp:Content>

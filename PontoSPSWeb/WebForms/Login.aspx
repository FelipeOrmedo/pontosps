﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PontoSPSWeb.WebForms.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0/">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc."/>
    <meta name="author" content="Coderthemes"/>

    <link rel="shortcut icon" href="../images/favicon_1.ico"/>

    <title>Sistema de Ponto SPS Consultoria</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
    <link href="../css/components.css" rel="stylesheet" type="text/css" />
    <link href="../css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../css/Estilo.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class=" card-box">
                <div class="row">
                    <div class="row bg-azul">
                        <h3 class="text-center">
                            <img src="../Imagens/logo.png" /></h3>
                    </div>
                </div>

                <div class="panel-body">
                    <asp:Login ID="Login1" runat="server" Width="100%" OnAuthenticate="Login1_Authenticate">
                        <LayoutTemplate>
                            <div class="row">
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <asp:TextBox ID="UserName" runat="server" class="form-control" type="text" required="" placeholder="E-Mail"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group marginTop">
                                    <div class="col-xs-12">
                                        <asp:TextBox ID="Password" runat="server" class="form-control" type="password" required="" placeholder="Senha"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group text-center marginTop">
                                    <div class="col-xs-12">
                                        <asp:LinkButton ID="LoginButton" CssClass="btn btn-block btn-lg btn-primary waves-effect waves-light" runat="server" CommandName="Login" ValidationGroup="Login1">Logar</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group text-center marginTop">
                                    <div class="col-xs-12">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </div>
                                </div>
                            </div>

                        </LayoutTemplate>
                    </asp:Login>
                </div>
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/detect.js"></script>
        <script src="../js/fastclick.js"></script>
        <script src="../js/jquery.slimscroll.js"></script>
        <script src="../js/jquery.blockUI.js"></script>
        <script src="../js/waves.js"></script>
        <script src="../js/wow.min.js"></script>
        <script src="../js/jquery.nicescroll.js"></script>
        <script src="../js/jquery.scrollTo.min.js"></script>


        <script src="../js/jquery.core.js"></script>
        <script src="../js/jquery.app.js"></script>
    </form>
</body>
</html>

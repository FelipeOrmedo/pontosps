﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PontoSPSWeb.WebForms
{
    public partial class Funcionario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //carregaGridLabel.Text = Dao.Funcionarios.CarregaDados();
                Dao.FuncionariosDao.CarregaGridView(GridView1);
                //GridView1.Columns[0].HeaderText = "Código";
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mensagemLabel.Text = string.Empty;
            if (IsPostBack)
            {
                txtCod.Text = GridView1.SelectedRow.Cells[1].Text;
                txtCode.Text = GridView1.SelectedRow.Cells[2].Text;
                txtNome.Text = Context.Server.HtmlDecode(GridView1.SelectedRow.Cells[3].Text);
                txtSetor.Text = GridView1.SelectedRow.Cells[4].Text;
                excluirLinkButton.Visible = true;
            }
        }

        protected void salvarLinkButton_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtSetor.Text))
            {
                mensagemLabel.Text = Support.SystemMessage.Info("Campos <strong>Nome</strong> e <strong>Setor</strong> obrigatórios");
                return;
            }

            var model = new Model.FuncionarioModel();

            model.Nome = txtNome.Text;
            model.Setor = txtSetor.Text;

            if (string.IsNullOrEmpty(txtCod.Text))
            {
                try
                {
                    Dao.FuncionariosDao.Save(model);
                    mensagemLabel.Text = Support.SystemMessage.Sucess("Funcionário cadastrado com sucesso!");
                    Dao.FuncionariosDao.CarregaGridView(GridView1);
                    LimpaCampos();
                }
                catch (Exception ex)
                {
                    mensagemLabel.Text = Support.SystemMessage.Danger("Erro ao cadastrar Funcionario: " + ex.Message);
                    Dao.FuncionariosDao.CarregaGridView(GridView1);
                    LimpaCampos();
                }
            }
            else
            {
                try
                {
                    model.Id = Convert.ToInt32(txtCod.Text);
                    Dao.FuncionariosDao.Update(model);
                    mensagemLabel.Text = Support.SystemMessage.Sucess("Funcionário alterado com sucesso!");
                    Dao.FuncionariosDao.CarregaGridView(GridView1);
                    LimpaCampos();
                }
                catch (Exception ex)
                {
                    mensagemLabel.Text = Support.SystemMessage.Danger("Erro ao alterar Funcionario: " + ex.Message);
                    LimpaCampos();
                }
            }
            excluirLinkButton.Visible = false;
        }

        protected void cancelarLinkButton_Click(object sender, EventArgs e)
        {
            LimpaCampos();
            excluirLinkButton.Visible = false;
        }

        private void LimpaCampos()
        {
            txtCod.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtSetor.Text = string.Empty;
        }

        protected void excluirLinkButton_Click(object sender, EventArgs e)
        {
            var model = new Model.FuncionarioModel();

            model.Nome = txtNome.Text;
            model.Setor = txtSetor.Text;

            if (!string.IsNullOrEmpty(txtCod.Text))
            {
                try
                {
                    model.Id = Convert.ToInt32(txtCod.Text);
                    Dao.FuncionariosDao.Delete(model);
                    mensagemLabel.Text = Support.SystemMessage.Sucess("Funcionário Excluído com sucesso!");
                    Dao.FuncionariosDao.CarregaGridView(GridView1);
                    LimpaCampos();
                }
                catch (Exception ex)
                {
                    mensagemLabel.Text = Support.SystemMessage.Danger("Erro ao Excluir Funcionario: " + ex.Message);
                    Dao.FuncionariosDao.CarregaGridView(GridView1);
                    LimpaCampos();
                }
            }
            excluirLinkButton.Visible = false;
        }
    }
}
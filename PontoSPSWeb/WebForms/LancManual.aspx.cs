﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PontoSPSWeb.WebForms
{
    public partial class LancManual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dao.VerLancamentoDao.BuscaFuncionarios(funcionariosDropDownList);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            funcionarioLabel.Text = string.Empty;
            //linhasLabel.Text = string.Empty;
            if (string.IsNullOrEmpty(funcionariosDropDownList.SelectedItem.Text))
            {
                funcionarioLabel.Text = Support.StaticMessage.Danger("Selecione um funcionário para realizar a busca");
                return;
            }

            LimpaCampos();

            funcionarioLabel.Text = Support.StaticMessage.Info("<strong>FUNCIONÁRIO: </strong>" + funcionariosDropDownList.SelectedItem.Text);

            Dao.LancManualDao.ListaLancManual(funcionariosDropDownList.SelectedValue, DropDownList1.Text, GridView1);
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mensagemLabel.Text = string.Empty;
            if (IsPostBack)
            {
                bool HabilitaBotao = false;

                txtDia.Text = GridView1.SelectedRow.Cells[1].Text;
                txtEntrada.Text = GridView1.SelectedRow.Cells[2].Text;
                txtSaidaAlmoco.Text = GridView1.SelectedRow.Cells[3].Text;
                txtRetornoAlmoco.Text = GridView1.SelectedRow.Cells[4].Text;
                txtSaida.Text = GridView1.SelectedRow.Cells[5].Text;

                Model.LancManualModel model = new Model.LancManualModel();
                model.IdFunc = funcionariosDropDownList.SelectedValue;
                string dia = Convert.ToDateTime(txtDia.Text).ToString("yyyy-MM-dd");
                txtDia.ReadOnly = true;

                txtObs.Text = Dao.LancManualDao.obs(model, dia);

                model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtEntrada.Text));
                if (Dao.LancManualDao.LancAutomatico(model))
                    txtEntrada.ReadOnly = true;
                else
                {
                    txtEntrada.ReadOnly = false;
                    HabilitaBotao = true;
                }

                model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtSaidaAlmoco.Text));
                if (Dao.LancManualDao.LancAutomatico(model))
                    txtSaidaAlmoco.ReadOnly = true;
                else
                {
                    txtSaidaAlmoco.ReadOnly = false;
                    HabilitaBotao = true;
                }

                model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtRetornoAlmoco.Text));
                if (Dao.LancManualDao.LancAutomatico(model))
                    txtRetornoAlmoco.ReadOnly = true;
                else
                {
                    txtRetornoAlmoco.ReadOnly = false;
                    HabilitaBotao = true;
                }

                model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtSaida.Text));
                if (Dao.LancManualDao.LancAutomatico(model))
                    txtSaida.ReadOnly = true;
                else
                {
                    txtSaida.ReadOnly = false;
                    HabilitaBotao = true;
                }

                if (HabilitaBotao == true)
                    salvarLinkButton.Visible = true;
                else
                    salvarLinkButton.Visible = false;   
            }
        }

        protected void salvarLinkButton_Click(object sender, EventArgs e)
        {
            Model.LancManualModel model = new Model.LancManualModel();
            model.IdFunc = funcionariosDropDownList.SelectedValue;
            model.obs = txtObs.Text;

            string dia = Convert.ToDateTime(txtDia.Text).ToString("yyyy-MM-dd");

            model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtEntrada.Text));
            if (!Dao.LancManualDao.LancAutomatico(model))
            {
                Dao.LancManualDao.Data = Convert.ToDateTime(string.Concat(dia, " ", GridView1.SelectedRow.Cells[2].Text));
                if (Dao.LancManualDao.NovoRegistro(model))
                    Dao.LancManualDao.Save(model);
                else
                    Dao.LancManualDao.Update(model, dia);
            }

            model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtSaidaAlmoco.Text));
            if (!Dao.LancManualDao.LancAutomatico(model))
            {
                Dao.LancManualDao.Data = Convert.ToDateTime(string.Concat(dia, " ", GridView1.SelectedRow.Cells[3].Text));
                if (Dao.LancManualDao.NovoRegistro(model))
                    Dao.LancManualDao.Save(model);
                else
                    Dao.LancManualDao.Update(model, dia);
            }

            model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtRetornoAlmoco.Text));
            if (!Dao.LancManualDao.LancAutomatico(model))
            {
                Dao.LancManualDao.Data = Convert.ToDateTime(string.Concat(dia, " ", GridView1.SelectedRow.Cells[4].Text));
                if (Dao.LancManualDao.NovoRegistro(model))
                    Dao.LancManualDao.Save(model);
                else
                    Dao.LancManualDao.Update(model, dia);
            }

            model.Data = Convert.ToDateTime(string.Concat(dia, " ", txtSaida.Text));
            if (!Dao.LancManualDao.LancAutomatico(model))
            {
                Dao.LancManualDao.Data = Convert.ToDateTime(string.Concat(dia, " ", GridView1.SelectedRow.Cells[5].Text));
                if (Dao.LancManualDao.NovoRegistro(model))
                    Dao.LancManualDao.Save(model);
                else
                    Dao.LancManualDao.Update(model, dia);
            }

            mensagemLabel.Text = Support.SystemMessage.Sucess("Informações Salva Com Sucesso");
            Dao.LancManualDao.ListaLancManual(funcionariosDropDownList.SelectedValue, DropDownList1.Text, GridView1);

            LimpaCampos();
        }

        private void LimpaCampos()
        {
            txtDia.Text = string.Empty;
            txtEntrada.Text = string.Empty;
            txtSaidaAlmoco.Text = string.Empty;
            txtRetornoAlmoco.Text = string.Empty;
            txtSaida.Text = string.Empty;
            txtObs.Text = string.Empty;
        }
    }
}
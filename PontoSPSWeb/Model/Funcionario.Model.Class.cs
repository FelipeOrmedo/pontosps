﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Model
{
    public class FuncionarioModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Setor { get; set; }
    }
}
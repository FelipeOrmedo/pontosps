﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Model
{
    public class ImportModel
    {
        public int IdUserArquivo { get; set; }
        public int Mchn { get; set; }
        public string NumPonto { get; set; }
        public string Nome { get; set; }
        public string Mode { get; set; }
        public string IOMd { get; set; }
        public DateTime Data { get; set; }
    }
}
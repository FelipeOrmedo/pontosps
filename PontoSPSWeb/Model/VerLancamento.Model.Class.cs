﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Model
{
    public static class VerLancamentoModel
    {
        public static TimeSpan tsTotalHoras { get; set; }
        public static TimeSpan tsTotalRendimento { get; set; }
        public static TimeSpan tsTotalDesconto { get; set; }

        public static string TotalHoras { get; set; }
        public static string TotalRendimento { get; set; }
        public static string TotalDesconto { get; set; }
    }
}
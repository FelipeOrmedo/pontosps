﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Dao
{
    public static class LancManualDao
    {
        public static int Id { get; set; }
        public static DateTime Data { get; set; }

        #region LISTA LANÇAMENTO MANUAL
        public static ArrayList ListaLancManual(string ID, string periodo, System.Web.UI.WebControls.GridView gridView)
        {
            ArrayList array = new ArrayList();
            ArrayList arrayDia = new ArrayList();
            DateTime dtInicio = Convert.ToDateTime("01-" + periodo).AddHours(0).AddMinutes(0).AddSeconds(0);
            DateTime dtFim = Convert.ToDateTime("01-" + periodo).AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);

            bool atualizarDia = true;
            string dia = "";
            int contador = 0;

            DataTable dt = new DataTable();
            dt.Columns.Add("Data");
            dt.Columns.Add("hrEntrada");
            dt.Columns.Add("hrSaidaAlmoco");
            dt.Columns.Add("hrRetornoAlmoco");
            dt.Columns.Add("hrHoraSaida");
            DataRow dr;

            string Row = string.Empty;

            int Id = Convert.ToInt32(ID);
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var code = (from q in db.Funcionario where q.IdFuncionario == Id select q).Single();


                    var query = (from q in db.Ponto where q.NumPonto == code.Codigo && q.Data >= dtInicio && q.Data <= dtFim orderby q.Data ascending select q).ToList();

                    foreach (var item in query)
                    {
                        arrayDia.Add(item.Data);
                    }

                    foreach (var dias in arrayDia)
                    {
                        if (dia != dias.ToString().Substring(0, 2))
                        {
                            atualizarDia = true;
                        }

                        if (atualizarDia == true)
                        {
                            dia = dias.ToString().Substring(0, 2);
                            atualizarDia = false;

                            if (!string.IsNullOrEmpty(Row))
                            {
                                while (contador <= 4)
                                {
                                    Row += "00:00:00";
                                    contador++;
                                }
                            }

                            if (contador == 5)
                            {
                                array.Add(Row);
                            }

                            contador = 0;
                            Row = string.Empty;
                        }
                        if (contador != 4)
                            contador += 1;

                        if (contador == 1)
                        {
                            Row += dia;
                            Row += dias.ToString().Substring(11, 8);
                        }
                        if (contador == 2)
                        {
                            Row += dias.ToString().Substring(11, 8);
                        }

                        if (contador == 3)
                        {
                            Row += dias.ToString().Substring(11, 8);
                        }

                        if (contador == 4)
                        {
                            Row += dias.ToString().Substring(11, 8);
                        }
                    }

                    foreach (var item in array)
                    {
                        dr = dt.NewRow();
                        dr["Data"] = (item.ToString().Substring(0, 2) + "-" + periodo).Replace("-", "/");
                        dr["hrEntrada"] = item.ToString().Substring(2, 8);
                        dr["hrSaidaAlmoco"] = item.ToString().Substring(10, 8);
                        dr["hrRetornoAlmoco"] = item.ToString().Substring(18, 8);
                        dr["hrHoraSaida"] = item.ToString().Substring(26, 8);
                        dt.Rows.Add(dr);
                    }

                    gridView.DataSource = dt;
                    gridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return array;
        }
        #endregion

        #region VALIDA SE  REGISTRO JÁ EXISTE
        public static bool NovoRegistro(Model.LancManualModel model)
        {
            bool Novo = false;
            int ID = Convert.ToInt32(model.IdFunc);
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var func = (from q in db.Funcionario.Where(w => w.IdFuncionario == ID) select q).Single();

                    var query = (from q in db.Ponto.Where(w => w.NumPonto == func.Codigo && w.Data == Data) select q).ToList();

                    if (query.Count > 0)
                    {
                        Novo = false;

                        foreach (var item in query)
                        {
                            Id = item.IdPonto;
                        }
                    }
                    else
                        Novo = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Novo;
        }
        #endregion

        #region VALIDA SE É LANÇAMENTO AUTOMATICO
        public static bool LancAutomatico(Model.LancManualModel model)
        {
            bool Automatico = false;
            int ID = Convert.ToInt32(model.IdFunc);
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var func = (from q in db.Funcionario.Where(w => w.IdFuncionario == ID) select q).Single();

                    var query = (from q in db.Ponto.Where(w => w.NumPonto == func.Codigo && w.Data == model.Data) select q).ToList();

                    if (query.Count > 0)
                    {
                        foreach (var item in query)
                        {
                            if (item.Mode == "A")
                                Automatico = true;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Automatico;
        }
        #endregion

        #region SELECIONA OBS
        public static string obs(Model.LancManualModel model, string dia)
        {
            string obs = string.Empty;
            DateTime data = Convert.ToDateTime(dia);
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Observacao.Where(w => w.IdFunc == model.IdFunc && w.Data == data) select q).ToList();

                    if (query.Count > 0)
                    {
                        foreach (var item in query)
                        {
                            obs = item.Obs;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return obs;
        }
        #endregion

        #region SAVE
        public static void Save(Model.LancManualModel model)
        {
            int ID = Convert.ToInt32(model.IdFunc);

            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Funcionario.Where(w => w.IdFuncionario == ID) select q).Single();

                    Ponto p = new Ponto();
                    p.IdUserArquivo = 0;
                    p.Mchn = 1;
                    p.NumPonto = query.Codigo;
                    p.Nome = query.Nome;
                    p.Mode = "M";
                    p.IOMd = "1";
                    p.Data = model.Data;
                    db.Ponto.Add(p);
                    db.SaveChanges();

                    Observacao o = new Observacao();
                    o.IdFunc = query.IdFuncionario.ToString();
                    o.Data = Convert.ToDateTime(model.Data.ToShortDateString());
                    o.Obs = model.obs;
                    db.Observacao.Add(o);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UPDATE
        public static void Update(Model.LancManualModel model, string dia)
        {
            int ID = Convert.ToInt32(model.IdFunc);
            DateTime data = Convert.ToDateTime(dia);
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Funcionario.Where(w => w.IdFuncionario == ID) select q).Single();

                    var query2 = (from q in db.Ponto.Where(w => w.NumPonto == query.Codigo && w.Data == Data) select q).Single();

                    query2.Data = model.Data;

                    var obs = (from q in db.Observacao.Where(w => w.IdFunc == model.IdFunc && w.Data == data) select q).Single();

                    obs.Obs = model.obs;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
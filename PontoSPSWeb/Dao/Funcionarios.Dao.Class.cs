﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PontoSPSWeb.Dao
{
    public static class FuncionariosDao
    {
        public static string CarregaDados()
        {
            StringBuilder tabela = new StringBuilder();

            using (var db = new PontoSPSEntities())
            {
                var query = (from q in db.Funcionario select q).ToList();

                foreach (var item in query)
                {
                    tabela.AppendLine("<tr class='gradeX'>");
                    tabela.AppendLine("<td>" + item.IdFuncionario + "</td>");
                    tabela.AppendLine("<td>" + item.Nome + "</td>");
                    tabela.AppendLine("<td>" + item.Cargo + "</td>");
                    tabela.AppendLine("<td class='actions'>");
                    tabela.AppendLine("<asp:LinkButton ID = 'salvarLinkButton' runat='server' class='hidden on-editing save-row'><i class='fa fa-save'></i></asp:LinkButton>");
                    tabela.AppendLine("<asp:LinkButton ID = 'cancelarLinkButton' runat='server' class='hidden on-editing cancel-row'><i class='fa fa-times'></i></asp:LinkButton>");
                    tabela.AppendLine("<asp:LinkButton ID = 'editarLinkButton' runat='server' class='on-default on-editing edit-row'><i class='fa fa-pencil'></i></asp:LinkButton>");
                    tabela.AppendLine("<asp:LinkButton ID = 'removerLinkButton' runat='server' class='on-default on-editing remove-row'><i class='fa fa-trash-o'></i></asp:LinkButton>");
                    tabela.AppendLine("</td>");
                    tabela.AppendLine("</tr>");
                }
            }
            return tabela.ToString();
        }

        public static void CarregaGridView(System.Web.UI.WebControls.GridView gridView)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = from q in db.Funcionario select q;

                    gridView.DataSource = query.ToList();
                    gridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void Save(Model.FuncionarioModel model)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var Func = new Funcionario();
                    Func.Nome = model.Nome;
                    Func.Cargo = model.Setor;
                    db.Funcionario.Add(Func);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Update(Model.FuncionarioModel model)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    int ID = model.Id;
                    var query = (from q in db.Funcionario.Where(w => w.IdFuncionario == ID) select q).Single();
                    query.Nome = model.Nome;
                    query.Cargo = model.Setor;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static void Delete(Model.FuncionarioModel model)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    int ID = model.Id;
                    var query = (from q in db.Funcionario.Where(w => w.IdFuncionario == ID) select q).Single();
                    db.Funcionario.Remove(query);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
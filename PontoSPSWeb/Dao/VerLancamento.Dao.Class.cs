﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace PontoSPSWeb.Dao
{
    public static class VerLancamentoDao
    {
        public static string Busca(string txtNome, string txtPeriodo)
        {
            StringBuilder str = new StringBuilder();
            ArrayList array = new ArrayList();
            ArrayList arrayHora = new ArrayList();
            ArrayList arrayRendimento = new ArrayList();
            ArrayList arrayDesconto = new ArrayList();

            DateTime dtinicio = Convert.ToDateTime("01-" + txtPeriodo);
            DateTime dtFim = Convert.ToDateTime("01-" + txtPeriodo).AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);

            using (var db = new PontoSPSEntities())
            {
                var code = (from q in db.Funcionario where q.Nome.Contains(txtNome) select q).Single();

                var query = (from q in db.Ponto where q.NumPonto == code.Codigo && q.Data >= dtinicio && q.Data <= dtFim orderby q.Data ascending select q).ToList();

                foreach (var item in query)
                {
                    array.Add(item.Data);
                }
            }

            bool atualizadDia = true;
            string dia = "";
            int contador = 0;
            DateTime hrInicio = DateTime.Now;
            DateTime hrInicioAlmoco = DateTime.Now;
            DateTime hrFimAlmoco = DateTime.Now;
            DateTime hrSaida = DateTime.Now;
            DateTime TotalHoras;
            TimeSpan totalRendimento = new TimeSpan();
            TimeSpan HorasDia = new TimeSpan(8, 0, 0);

            foreach (var item in array)
            {
                if (dia != item.ToString().Substring(0, 2))
                {
                    atualizadDia = true;
                    contador = 0;
                }

                if (atualizadDia == true)
                {
                    dia = item.ToString().Substring(0, 2);
                    atualizadDia = false;
                    str.AppendLine("<tr>");
                    str.AppendLine("<td>" + item.ToString().Substring(0, 10) + "</td>");
                }
                contador += 1;
                if (contador == 1)
                {
                    str.AppendLine("<td>" + item.ToString().Substring(11, 8) + "</td>");
                    hrInicio = Convert.ToDateTime(item.ToString());
                }
                if (contador == 2)
                {
                    if (item.ToString().Substring(11, 8) == "")
                        str.AppendLine("<td></td>");
                    else
                    {
                        str.AppendLine("<td>" + item.ToString().Substring(11, 8) + "</td>");
                        hrInicioAlmoco = Convert.ToDateTime(item.ToString());
                    }
                }

                if (contador == 3)
                {
                    if (item.ToString().Substring(11, 8) == "")
                        str.AppendLine("<td></td>");
                    else
                    {
                        str.AppendLine("<td>" + item.ToString().Substring(11, 8) + "</td>");
                        hrFimAlmoco = Convert.ToDateTime(item.ToString());
                    }
                }

                if (contador == 4)
                {
                    if (item.ToString().Substring(11, 8) == "")
                        str.AppendLine("<td></td>");
                    else
                    {
                        str.AppendLine("<td>" + item.ToString().Substring(11, 8) + "</td>");
                        hrSaida = Convert.ToDateTime(item.ToString());
                    }

                    string tempoTolerancia = Tolerancia();

                    #region VALIDA TOLERANCIA DE ENTRADA
                    if (Convert.ToDateTime(hrInicio.ToString().Substring(11, 8)) >= Convert.ToDateTime(Convert.ToDateTime("08:00:00").ToString().Substring(11, 8)) && Convert.ToDateTime(hrInicio.ToString().Substring(11, 8)) <= Convert.ToDateTime(Convert.ToDateTime("08:" + tempoTolerancia + ":00").ToString().Substring(11, 8)))
                    {
                        TotalHoras = hrInicio.AddMinutes(-hrInicio.Minute).AddSeconds(-hrInicio.Second);
                        //HoraFinal.Subtract(TotalHoras);
                    }

                    if (Convert.ToDateTime(hrInicio.ToString().Substring(11, 8)) >= Convert.ToDateTime(Convert.ToDateTime("09:00:00").ToString().Substring(11, 8)) && Convert.ToDateTime(hrInicio.ToString().Substring(11, 8)) <= Convert.ToDateTime(Convert.ToDateTime("09:" + tempoTolerancia + ":00").ToString().Substring(11, 8)))
                    {
                        TotalHoras = hrInicio.AddMinutes(-hrInicio.Minute).AddSeconds(-hrInicio.Second);
                    }
                    #endregion

                    TimeSpan TotalAlmoco = hrFimAlmoco.Subtract(hrInicioAlmoco);
                    
                    if (TotalAlmoco >= new TimeSpan(01, 00, 00) && TotalAlmoco <= new TimeSpan(01, 10, 00) || TotalAlmoco < new TimeSpan(01, 00, 00))
                    {
                        TotalAlmoco = new TimeSpan(01, 00, 00);
                    }
                        TimeSpan TotalTrabalhado = hrSaida.Subtract(hrInicio);

                    TotalTrabalhado = TotalTrabalhado.Subtract(TotalAlmoco);
                    arrayHora.Add(TotalTrabalhado);

                    if (TotalTrabalhado > HorasDia)
                    {
                        totalRendimento = TotalTrabalhado.Subtract(HorasDia);
                        str.AppendLine("<td>"+ totalRendimento + "</td>");
                        str.AppendLine("<td>00:00:00</td>");
                        str.AppendLine("</tr>");
                        arrayRendimento.Add(totalRendimento);
                    }

                    if (TotalTrabalhado < HorasDia)
                    {
                        totalRendimento = TotalTrabalhado.Subtract(HorasDia);
                        str.AppendLine("<td>00:00:00</td>");
                        str.AppendLine("<td>"+ totalRendimento + "</td>");
                        str.AppendLine("</tr>");
                        arrayDesconto.Add(totalRendimento);
                    }

                    
                }
            }

            if (array.Count > 0)
            {
                #region CALCULA TOTAL DE HORAS
                //ZERANDO O TIMESPAN
                Model.VerLancamentoModel.tsTotalHoras = TimeSpan.Parse("00:00:00");
                Model.VerLancamentoModel.TotalHoras = "00:00:00";
                foreach (var item in arrayHora)
                {
                    Model.VerLancamentoModel.tsTotalHoras += TimeSpan.Parse(item.ToString());
                }

                int totalDias = Convert.ToInt32(Model.VerLancamentoModel.tsTotalHoras.ToString().Substring(0, 1));
                int totalHoras = Convert.ToInt32(Model.VerLancamentoModel.tsTotalHoras.ToString().Substring(2, 2));

                totalDias = totalDias * 24;
                totalHoras = totalHoras + totalDias;
                Model.VerLancamentoModel.TotalHoras = string.Concat(totalHoras.ToString(), ":", Model.VerLancamentoModel.tsTotalHoras.ToString().Substring(5, 2), ":", Model.VerLancamentoModel.tsTotalHoras.ToString().Substring(8, 2));
                #endregion

                #region CALCULA TOTAL RENDIMENTO
                //ZERANDO O TIMESPAN
                Model.VerLancamentoModel.tsTotalRendimento = TimeSpan.Parse("00:00:00");
                int totalHorasRendimento = 0;
                int totalDiasRendimento = 0;

                Model.VerLancamentoModel.TotalRendimento = "00:00:00";

                foreach (var item in arrayRendimento)
                {
                    Model.VerLancamentoModel.tsTotalRendimento += TimeSpan.Parse(item.ToString());
                }

                if (Model.VerLancamentoModel.tsTotalRendimento.ToString().Substring(0, 3).Contains("."))
                {
                    totalDiasRendimento = Convert.ToInt32(Model.VerLancamentoModel.tsTotalRendimento.ToString().Substring(0, 1));
                    totalDiasRendimento = totalDiasRendimento * 24;
                    totalHorasRendimento = totalHorasRendimento + totalDiasRendimento;
                    totalHorasRendimento = Convert.ToInt32(Model.VerLancamentoModel.tsTotalRendimento.ToString().Substring(2, 2));
                }
                else
                {
                    totalHorasRendimento = Convert.ToInt32(Model.VerLancamentoModel.tsTotalRendimento.ToString().Substring(0, 2));
                }

                Model.VerLancamentoModel.TotalRendimento = string.Concat(totalHorasRendimento.ToString(), ":", Model.VerLancamentoModel.tsTotalRendimento.ToString().Substring(3, 2), ":", Model.VerLancamentoModel.tsTotalRendimento.ToString().Substring(6, 2));
                #endregion

                #region CALCULA TOTAL DESCONTO
                //ZERANDO O TIMESPAN
                Model.VerLancamentoModel.tsTotalDesconto = TimeSpan.Parse("00:00:00");
                int totalHorasDesconto = 0;
                int totalDiasDesconto = 0;

                Model.VerLancamentoModel.TotalDesconto = "00:00:00";

                foreach (var item in arrayDesconto)
                {
                    Model.VerLancamentoModel.tsTotalDesconto += TimeSpan.Parse(item.ToString());
                }

                if (Model.VerLancamentoModel.tsTotalDesconto.ToString().Substring(0, 3).Contains("."))
                {
                    totalDiasDesconto = Convert.ToInt32(Model.VerLancamentoModel.tsTotalDesconto.ToString().Substring(0, 1));
                    totalDiasDesconto = totalDiasDesconto * 24;
                    totalHorasDesconto = totalHorasDesconto + totalDiasDesconto;
                    totalHorasDesconto = Convert.ToInt32(Model.VerLancamentoModel.tsTotalDesconto.ToString().Substring(2, 2));
                }
                else
                {
                    totalHorasDesconto = Convert.ToInt32(Model.VerLancamentoModel.tsTotalDesconto.ToString().Substring(1, 2));
                }

                string totalDesc = totalHorasDesconto.ToString() == "0" ? "00" : totalHorasDesconto.ToString();

                Model.VerLancamentoModel.TotalDesconto = string.Concat(totalDesc, ":", Model.VerLancamentoModel.tsTotalDesconto.ToString().Substring(4, 2), ":", Model.VerLancamentoModel.tsTotalDesconto.ToString().Substring(7, 2));
                #endregion
            }

            return str.ToString();
        }
        
        #region TEMPO DE TOLERANCIA
        public static string Tolerancia()
        {
            string tempotolerancia = string.Empty;
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Tolerancia select q).Single();

                    tempotolerancia = query.Tempo;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return tempotolerancia;
        }
        #endregion

        #region BUSCA FUNCIONARIOS
        public static void BuscaFuncionarios(DropDownList dropDownList)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = from q in db.Funcionario
                                orderby q.Nome
                                select new
                                {
                                    q.IdFuncionario,
                                    q.Nome
                                };
                    dropDownList.DataTextField = "Nome";
                    dropDownList.DataValueField = "IdFuncionario";
                    dropDownList.DataSource = query.ToList();
                    dropDownList.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
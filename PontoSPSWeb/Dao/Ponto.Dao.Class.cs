﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Dao
{
    public static class PontoDao
    {
        public static void Save(Model.ImportModel Model)
        {
            using (var db = new PontoSPSEntities())
            {
                Ponto p = new Ponto();
                p.IdUserArquivo = Model.IdUserArquivo;
                p.Mchn = Model.Mchn;
                p.NumPonto = Model.NumPonto;
                p.Nome = Model.Nome;
                p.IOMd = Model.IOMd;
                p.Mode = Model.Mode;
                p.Data = Model.Data;
                db.Ponto.Add(p);
                db.SaveChanges();
            }
        }

        public static void Update(Model.ImportModel Model)
        {
            int ID = Model.IdUserArquivo;

            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Ponto.Where(w => w.IdUserArquivo == ID) select q).Single();

                    query.IdUserArquivo = Model.IdUserArquivo;
                    query.Mchn = Model.Mchn;
                    query.NumPonto = Model.NumPonto;
                    query.Nome = Model.Nome;
                    query.IOMd = Model.IOMd;
                    query.Mode = Model.Mode;
                    query.Data = Model.Data;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool ValidaExiste(Model.ImportModel Model)
        {
            int ID = Model.IdUserArquivo;
            bool retorno = false;

            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Ponto.Where(w => w.IdUserArquivo == ID) select q).ToList();

                    if (query.Count > 0)
                        retorno = true;
                    else
                        retorno = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return retorno;
        }
    }
}
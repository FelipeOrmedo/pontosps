﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Dao
{
    public class LoginDao
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public bool Login()
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    string senha = Support.Criptografy.GerarHashMd5(Password);
                    var query = (from q in db.Login where q.Email == Email && q.Senha == senha select q).ToList();

                    if (query.Count > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Dao
{
    public static class CadUsuarioDao
    {
        public static void CarregaGridView(System.Web.UI.WebControls.GridView gridView)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = from q in db.Login select q;

                    gridView.DataSource = query.ToList();
                    gridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void Save(Model.CadUsuarioModel model)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    Login l = new Login();
                    l.Nome = model.Nome;
                    l.Email = model.Email;
                    l.Senha = model.Senha;
                    db.Login.Add(l);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Update(Model.CadUsuarioModel model)
        {
            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Login where q.IdLogin == model.ID select q).Single();
                    query.Nome = model.Nome;
                    query.Email = model.Email;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(Model.CadUsuarioModel model)
        {
            int ID = model.ID;

            try
            {
                using (var db = new PontoSPSEntities())
                {
                    var query = (from q in db.Login where q.IdLogin == ID select q).Single();
                    db.Login.Remove(query);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
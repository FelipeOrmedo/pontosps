﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontoSPSWeb.Support
{
    public static class SystemMessage
    {
        public static string Sucess(string Mensagem)
        {
            string msg = "<div class='alert alert-success alert-dismissable'>" +
                            " <button type = 'button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
                        Mensagem +
                    "</div>";
            return msg;
        }

        public static string Alert(string Mensagem)
        {
            string msg = "<div class='alert alert-warning alert-dismissable'>" +
                            " <button type = 'button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
                        Mensagem +
                    "</div>";
            return msg;
        }
        public static string Info(string Mensagem)
        {
            string msg = "<div class='alert alert-info alert-dismissable'>" +
                            " <button type = 'button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
                        Mensagem +
                    "</div>";
            return msg;
        }
        public static string Danger(string Mensagem)
        {
            string msg = "<div class='alert alert-danger alert-dismissable'>" +
                            " <button type = 'button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" +
                        Mensagem +
                    "</div>";
            return msg;
        }
    }
}